import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-data-personal",
  templateUrl: "./data-personal.component.html",
  styleUrls: ["./data-personal.component.scss"]
})
export class DataPersonalComponent implements OnInit {
  //Type de Data
  name: string = "Jose Miquilena";
  age: number;
  address: {
    street: string;
    city: string;
  };
  hobbies: string[];

  constructor() {
    this.age = 30;
    this.address = {
      street: "Los Alacranes Mzn 111 Casa 20 San Felix",
      city: "Ciudad Guayana"
    };
    this.hobbies = ["write", "read", "games"];
  }
  ngOnInit() {}
}
