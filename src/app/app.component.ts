import { Component } from "@angular/core";
import { DataService } from "./data.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  /**
   * Declarations Var
   */
  users = ["ryan", "joe", "cameron", "mary", "Carl"];

  //for form register user

  name: string = "Jose Miquilena";
  age: number = 30;

  //Post
  posts = [];

  //Type of Var
  activated: boolean = true;

  constructor(private dataService: DataService) {
    this.dataService.getData().subscribe(data => {
      this.posts = data;
    });
  }

  //functions
  SayHello = (name: string) => {
    alert("Hello " + name);
  };
  deleteUser = (name: string) => {
    for (let i = 0; i <= this.users.length; i++) {
      if (name === this.users[i]) {
        this.users.splice(i, 1); //elimina la posicion en el array
      }
    }
  };

  //se agrega un nuevo usuario
  addUser(newUser: any) {
    this.users.push(newUser.value);
    newUser.value = "";
    newUser.focus();
    return false;
  }
}
